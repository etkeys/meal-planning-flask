from flask import Flask, render_template
import json

app = Flask(__name__)

sample_schedule = json.loads('''[
    {
        "date": "2020-05-31",
        "days": [
            {
                "date": "2020-05-31",
                "dinner": {
                    "name": "Chicken curry",
                    "protein": "Chicken breast",
                    "veggies": ["Canned peas", "Black beans"],
                    "other_ingredients": ["Chicken bollion", "Tomato sauce",
                        "Chicken stock","Cream"]
                }
            },
            {
                "date": "2020-06-01",
                "lunch": {
                    "name": "Chicken curry",
                    "protein": "Chicken breast",
                    "veggies": ["Canned peas", "Black beans"],
                    "other_ingredients": ["Chicken bollion", "Tomatoe sauce",
                        "Chicken stock","Cream"]
                },
                "dinner": {
                    "name": "Hamburgers",
                    "protein": "Hamburgers",
                    "veggies": ["Canned green beans"]
                }
            },
            {
                "date": "2020-06-02",
                "lunch": {
                    "name": "Chicken curry",
                    "protein": "Chicken breast",
                    "veggies": ["Canned peas", "Black beans"],
                    "other_ingredients": ["Chicken bollion", "Tomatoe sauce",
                        "Chicken stock","Cream"]
                },
                "dinner": {
                    "name": "Beef ramen",
                    "protein": "Beef ramen",
                    "veggies": ["Canned green beans"]
                }
            },
            {
                "date": "2020-06-03",
                "lunch": {
                    "name": "Chicken curry",
                    "protein": "Chicken breast",
                    "veggies": ["Canned peas", "Black beans"],
                    "other_ingredients": ["Chicken bollion", "Tomatoe sauce",
                        "Chicken stock","Cream"]
                },
                "dinner": {
                    "name": "Steak and rice",
                    "protein": "Country style beef ribs",
                    "veggies": ["Canned corn", "Canned carrots"],
                    "other_ingredients": ["Rice", "Monteral marinade",
                        "Beef bolion"]
                }
            },
            {
                "date": "2020-06-04",
                "lunch": {
                    "name": "Steak and rice",
                    "protein": "Country style beef ribs",
                    "veggies": ["Canned corn", "Canned carrots"],
                    "other_ingredients": ["Rice", "Monteral marinade",
                        "Beef bolion"]
                },
                "dinner": {
                    "name": "Hot dogs",
                    "protein": "Hot dogs",
                    "veggies": ["Canned green beans"]
                }
            },
            {
                "date": "2020-06-05",
                "lunch": {
                    "name": "Steak and rice",
                    "protein": "Country style beef ribs",
                    "veggies": ["Canned corn", "Canned carrots"],
                    "other_ingredients": ["Rice", "Monteral marinade",
                        "Beef bolion"]
                }
            }
        ]
    }
]
''')

@app.route('/')
@app.route('/home')
@app.route('/schedule')
def schedule():
    data = get_current_schedule()
    return render_template('schedule.html', schedule=data)

def get_current_schedule():
    from datetime import date
    import calendar

    dto = sample_schedule
    for week in dto:
        for day in week['days']:
            old_parts = day['date'].split('-')
            old_date = date(
                int(old_parts[0]),
                int(old_parts[1]),
                int(old_parts[2]))
            day['date'] = calendar.day_name[old_date.weekday()]

    return dto


if __name__ == '__main__':
    app.run()